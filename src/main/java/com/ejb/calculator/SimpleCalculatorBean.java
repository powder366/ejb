package com.ejb.calculator;

import javax.ejb.LocalBean;
import javax.ejb.Remote;
import javax.ejb.Stateless;

@LocalBean
// Does not implement an interface therefore use @LocalBean instead of @Local
// when using interface also see
// https://stackoverflow.com/questions/7729905/what-is-local-remote-and-no-interface-view-in-ejb
@Remote
@Stateless
public class SimpleCalculatorBean extends CalculatorBeanBase {
}
