package com.ejb.calculator;

import javax.ejb.LocalBean;
import javax.ejb.Remote;
import javax.ejb.Stateful;

@LocalBean
@Remote
@Stateful
public class ComplexCalculatorBean extends CalculatorBeanBase {

    private int result = 0;

    public int complex(int... arguments) {
        // Should be a complex calculation, but only testing stateful bean
        for (int argument : arguments) {
            result += argument;
        }
        return result;
    }
}
