package com.ejb.calculator;

public class CalculatorBeanBase implements CalculatorCommon {
    public int add(int... arguments) {
        int result = 0;
        for (int argument : arguments) {
            result += argument;
        }
        return result;
    }
}
