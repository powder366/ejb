package com.ejb.first;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;

public class MyServiceMain {

    private static final String JNDI_NAME = "java:global/ejb-1.0-SNAPSHOT/MyService";

    public static void main(String[] args) {

        try {
            Context ctx = new InitialContext();
            IMyService myService = (IMyService) ctx.lookup(JNDI_NAME);
            String msg = myService.getMessage();
            System.out.println(msg);
        } catch (NamingException e) {
            e.printStackTrace();
        }
    }
}
