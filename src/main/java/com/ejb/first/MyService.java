package com.ejb.first;

import javax.ejb.Local;
import javax.ejb.Remote;
import javax.ejb.Stateless;

@Local
@Remote
@Stateless
public class MyService implements IMyService {
    public String getMessage() {
        return "Hello!";
    }
}
