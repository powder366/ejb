package com.ejb.mdb;
import javax.jms.*;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;

import static java.lang.System.exit;

public class MessageBeanMain {

    public static void main(String[] args) {
        String message = "Hej";
        try {
            Context ctx = new InitialContext();

            ConnectionFactory connectionFactory = (ConnectionFactory) ctx.lookup("jms/ConnectionFactory");
            Connection connection = connectionFactory.createConnection();
            connection.start();

            Session session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);

            Queue queue = (Queue) ctx.lookup("jms/messageQueue");
            MessageProducer messageProducer = session.createProducer(queue);

            TextMessage textMessage = session.createTextMessage(message);
            messageProducer.send(textMessage);

        } catch (NamingException | JMSException e) {
            System.out.println(e.fillInStackTrace());
            System.out.println("Error while trying to send message <" + message + ">");
            exit(1);
        }
        System.out.println("Message sent: " + message);
    }
}
