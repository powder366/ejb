package com.ejb.first;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import javax.ejb.embeddable.EJBContainer;
import javax.naming.Context;

import static org.junit.Assert.assertEquals;

public class MyServiceIntegrationTest {

    private static final String JNDI_NAME = "java:global/classes/MyService";

    private static Context ctx;
    private static EJBContainer ejbContainer;

    private static IMyService myService;

    @BeforeClass
    public static void setUpBeforeClass() throws Throwable {
        ejbContainer = EJBContainer.createEJBContainer();
        ctx = ejbContainer.getContext();
        myService = (IMyService) ctx.lookup(JNDI_NAME);
    }

    @AfterClass
    public static void tearDownAfterClass() {
        ejbContainer.close();
    }

    @Test
    public void getMessageTest() {
        assertEquals(myService.getMessage(), "Hello!");
    }
}
