package com.ejb.first;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class MyServiceTest {

    @Test
    public void getMessageTest() {
        MyService myService = new MyService();
        assertEquals(myService.getMessage(), "Hello!");
    }
}

