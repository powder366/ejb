package com.ejb.calculator;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import javax.ejb.embeddable.EJBContainer;
import javax.naming.Context;

import static junit.framework.TestCase.assertEquals;

public class ComplexIntegrationTest {

    private static final String JNDI_NAME = "java:global/classes/ComplexCalculatorBean";

    private static Context ctx;
    private static EJBContainer ejbContainer;

    private static ComplexCalculatorBean calc;

    @BeforeClass
    public static void obtainProxyReferences() throws Throwable {
        ejbContainer = EJBContainer.createEJBContainer();
        ctx = ejbContainer.getContext();
        calc = (ComplexCalculatorBean) ctx.lookup(JNDI_NAME);
    }

    @AfterClass
    public static void tearDownAfterClass() {
        ejbContainer.close();
    }

    @Test
    public void complex() {
        complex(calc);
    }

    private void complex(ComplexCalculatorBean calc) {
        int[] arguments = new int[] { 1 };
        int expectedSum = 2;
        int actualFirst = calc.complex(arguments);
        int actualFinal = calc.complex(arguments);

        assertEquals("Addition did not return the expected result", expectedSum, actualFinal);
    }
}
