package com.ejb.calculator;

import org.junit.Test;

import static junit.framework.TestCase.assertEquals;

public class SimpleUnitTest {

    @Test
    public void addition() {
        CalculatorCommon calc = new SimpleCalculatorBean();
        int[] arguments = new int[] { 2, 3, 5 };
        int expectedSum = 10;
        int actualSum = calc.add(arguments);
        assertEquals("Addition did not return the expected result", expectedSum, actualSum);
    }
}
