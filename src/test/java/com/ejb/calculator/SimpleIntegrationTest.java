package com.ejb.calculator;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import javax.ejb.embeddable.EJBContainer;
import javax.naming.Context;

import static junit.framework.TestCase.assertEquals;

public class SimpleIntegrationTest {

    private static final String JNDI_NAME = "java:global/classes/SimpleCalculatorBean";

    private static Context ctx;
    private static EJBContainer ejbContainer;

    private static CalculatorCommon calc;

    @BeforeClass
    public static void obtainProxyReferences() throws Throwable {
        ejbContainer = EJBContainer.createEJBContainer();
        ctx = ejbContainer.getContext();
        calc = (CalculatorCommon) ctx.lookup(JNDI_NAME);
    }

    @AfterClass
    public static void tearDownAfterClass() {
        ejbContainer.close();
    }

    @Test
    public void addition() {
        addition(calc);
    }

    private void addition(CalculatorCommon calc) {
        int[] arguments = new int[] { 2, 3, 5 };
        int expectedSum = 10;
        int actualSum = calc.add(arguments);

        assertEquals("Addition did not return the expected result", expectedSum, actualSum);
    }
}
