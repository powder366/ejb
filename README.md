asadmin delete-domain domain1
asadmin create-domain --adminport 4848 domain1

admin/admin

asadmin create-jms-resource --restype javax.jms.ConnectionFactory jms/ConnectionFactory
asadmin create-jms-resource --restype javax.jms.Queue jms/messageQueue

asadmin start-domain --verbose --debug 
asadmin stop-domain 
http://localhost:8080/    
http://localhost:4848/common/index.jsf    

mvn clean install -DskipTests
mvn package -DskipTests   
asadmin deploy target/ejb-1.0-SNAPSHOT.jar    
asadmin undeploy ejb-1.0-SNAPSHOT
